import { getElementConfig } from '@/components/EditorPage/utils';
import { Message } from 'element-ui';
import { merge } from 'loadsh';

export default {
  state: {
    // 项目数据
    projectData: {
      elements: [], // 页面容器集合
    },
    // 正在编辑的组件id
    editorActiveUuid: '',
    // 正在编辑的组件
    activeElement: null,
  },
  getters: {
    /**
     * 当前选中元素
     */
    activeElement(state) {
      if (!state.editorActiveUuid) {
        return {};
      }
      return state.activeElement;
    },
  },
  mutations: {
    // 点击左边按钮区域，添加画板节点容器(添加节点的方式一)
    ADD_ELEMENT(state, nodeData) {
      state.projectData.elements.push(nodeData);
    },
    // 设置当前选中的组件id
    SET_EDITOR_UUID(state, id) {
      state.editorActiveUuid = id;
      state.activeElement = state.projectData.elements.find((v) => v.uuid === id);
    },
    // 删除组件操作
    DELETE_ELEMENT(state, id) {
      const eleIndex = state.projectData.elements.findIndex((v) => v.uuid === id);
      state.projectData.elements.splice(eleIndex, 1);
      state.editorActiveUuid = '';
    },
    // 设置元素样式
    SET_ELEMENT_COMPONENT_Style(state, style) {
      const { activeElement } = state;
      activeElement.commonStyle = merge(activeElement.commonStyle, style);
      console.log(activeElement.commonStyle);
    },
  },
  actions: {
    // 新增组件
    addElement({ commit }, nodeData) {
      // 获取当前需要添加的元素节点的数据结构
      const newNodeData = getElementConfig(nodeData);
      console.log('要添加的数据', newNodeData);
      if (newNodeData) {
        // 装载到页面中
        commit('ADD_ELEMENT', newNodeData);
      }
    },
    // 设置当前选中的组件id
    async setActiveUuid({ commit }, id) {
      const success = true;
      return new Promise((resolve) => {
        commit('SET_EDITOR_UUID', id);
        setTimeout(() => {
          resolve({
            success,
          });
        }, 300);
      });
    },
    // 组件操作监听
    elementCommand({ dispatch, state }, command) {
      const elData = state.activeElement;
      switch (command) {
        case 'copy':
          dispatch('copyElement', elData);
          break;
        case 'delete':
          dispatch('deleteElement', elData.uuid);
          break;
        case 'fontA+':
          dispatch('resetElementCommonStyle', { fontSize: elData.commonStyle.fontSize + 1 });
          break;
        case 'fontA-':
          dispatch('resetElementCommonStyle', { fontSize: elData.commonStyle.fontSize - 1 });
          break;
        case 'fontB':
          dispatch('resetElementCommonStyle', { fontWeight: elData.commonStyle.fontWeight === 'bold' ? 'normal' : 'bold' });
          break;
        default:
          break;
      }
    },
    // 组件复制
    async copyElement({ state, dispatch }, elData) {
      const copyOrignData = elData || state.activeElement;
      try {
        const { success } = await dispatch('setActiveUuid', copyOrignData.uuid);
        if (success) {
          dispatch('addElement', copyOrignData);
        }
      } catch (e) {
        console.error(e);
        Message.error('请先选择组件');
      }
    },
    // 组件删除操作
    deleteElement({ state, commit }, uuid) {
      if (!uuid) {
        return false;
      }
      // 如果删除选中元素则取消元素选中
      if (uuid === state.editorActiveUuid) {
        commit('DELETE_ELEMENT');
      }
    },
    // 设置组件样式
    resetElementCommonStyle({ commit }, style) {
      commit('SET_ELEMENT_COMPONENT_Style', style);
    },
  },
};
