import editHeader from './editHeader';
import editPanel from './editPanel';
import editSilder from './editSilder';
import editRightPanel from './editRightPanel';

export {
  editHeader, editPanel, editRightPanel, editSilder,
};
