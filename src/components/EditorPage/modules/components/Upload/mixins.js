/*
 * @Date: 2020-06-05 19:23:04
 * @LastEditors: yang
 * @LastEditTime: 2020-06-13 14:13:59
 * @FilePath: \admin-shop\src\components\Upload\mixins\upload.js
 */

/**
 * @Description:  上传文件相关
 * @author yang
 * @date 2019/10/14
 */

export default {
  data() {
    return {
      // 文件上传接口地址
      uploadApi: 'http://yapi.gyang.live/mock/21/api-admin/upload',
      // 显示上传动画
      upLoading: false,
      uploadHeader: {},
    };
  },
  methods: {
    // 上传文件之前的钩子，参数为上传的文件
    beforeUpload(file) {
      const reg = /.+\./; // 获取文件后缀名
      const typeName = file.name.replace(reg, '');
      const isFormat = this.fileFormat.includes(typeName);
      const isLt2M = file.size / 1024 / 1024 < this.maxSize;
      if (!isFormat) {
        this.$notify.error({
          title: '上传失败',
          message: `格式不正确，只能上传${this.fileFormat.join('、')}格式文件`,
          duration: 80000,
        });
      }
      if (!isLt2M) {
        this.$notify.error({
          title: '上传失败',
          message: `上传文件大小不能超过${this.maxSize}M`,
          duration: 80000,
        });
      }
      if (isFormat && isLt2M) {
        this.$emit('upload-start', this.upLoading);
        this.upLoading = true;
      }
      return isFormat && isLt2M;
    },
    // 文件上传中的钩子
    onProgress() {},
    // 文件上传失败
    handleUploadError(err) {
      console.log(err);
      if (err.status === 401) {
        this.getUploadToken();
      }
      this.$notify.error({
        title: '上传失败',
        message: '文件上传失败，请重试',
        duration: 80000,
      });
      this.upLoading = false;
      this.$emit('upload-error', err);
    },
    // 文件上传成功
    handleUploadSuccess(res, file) {
      this.resetProgress();
      if (!res.success) {
        this.$message.error('图片上传失败');
        this.$emit('upload-error', err);
        return false;
      }
      const data = { ...res, file };
      this.uploadType === 'one' && (this.imageUrl = res.data);
      data.url = res.data;
      // 将上传成功后的响应数据传递至父组件
      this.$emit('getUploadData', data);
    },
    // 移除文件
    handleRemove(file, filelist) {
      // 解决el-upload中 filelist因单向数据流的原因不自动更新
      this.$refs.upload.uploadFiles = [...filelist];
      const list = [];
      filelist.forEach((item) => {
        if (item.response && item.response.key) {
          // 七牛云上传就应该手动拼接图片地址
          list.push(`${this.$store.state.viewApi}${item.response.key}`);
        } else {
          list.push(item.url);
        }
      });
      this.$emit('removeFileChange', list, file);
    },
    //  移除已上传的图片列表
    handleRemoveAll() {
      this.$refs.upload.clearFiles();
    },
    // 文件超出个数限制时的钩子
    onExceed(files) {
      console.log(files);
      this.$notify.error({
        title: '上传失败',
        message: `上传文件个数不能大于${this.maxLimit}个`,
        duration: 80000,
      });
    },
    // 预览图片
    handlePictureCardPreview(file) {
      this.imageUrl = file.url;
      this.dialogVisible = true;
    },
    // 重置加载动画
    resetProgress() {
      this.upLoading = false;
    },
    // 读取文件后缀名
    getfileTypeName(name) {
      if (name) {
        return name.substr(name.indexOf('/') + 1);
      }
      return name;
    },
  },
};
