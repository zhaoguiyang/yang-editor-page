import Text from './text';
import Img from './img';

export default {
  [Text.name]: Text,
  [Img.name]: Img,
};
