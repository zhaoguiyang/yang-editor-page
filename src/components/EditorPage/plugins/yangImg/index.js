import Component from './src/index';

/// / 为组件提供 install 方法，供组件对外按需引入
Component.install = (Vue) => {
  Vue.component(Component.name, Component);
};

export default Component;
