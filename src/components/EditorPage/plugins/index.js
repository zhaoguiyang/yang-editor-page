// 组件库入口

// 基础组件
import yangText from './yangText';
import yangImg from './yangImg';

// 导出组件列表

const components = [
  yangText,
  yangImg,
];

const install = function (Vue) {
  // 已经注册
  if (install.installed) return;
  install.installed = true;
  // 遍历所有注册的组件
  components.map((component) => Vue.component(component.name, component));
};
// vue挂载

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}
// 注册的组件对象
const registerComponents = {};

// 写入
components.forEach((item) => {
  registerComponents[item.name] = item;
});

export {
  yangText,
  registerComponents,
};

export default {
  install,
};
