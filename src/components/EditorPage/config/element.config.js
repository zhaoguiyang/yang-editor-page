/*
* 组件列表（左侧边栏配置）
*@param { String } title  组件类型标题
 *@param { Array } components 组件列表
 *@param { String } components.elName  组件名称（必须与组件内name保持一致）
 *@param { String } components.text  组件类型（必须与组件内name保持一致）
 *@param { String } components.title 组件标题（展示）
 * @param { String } components.icon 组件图标
 * @param { Object } components.defaultStyle 组件默认样式
* */

export default [
  // 基础组件
  {
    title: '基础组件',
    components: [
      {
        elName: 'yang-text',
        elType: 'text',
        title: '文本',
        icon: 'el-icon-s-grid',
        defaultStyle: {
          height: 100,
        },
      },
      {
        elName: 'yang-img',
        elType: 'image',
        title: '图片',
        icon: 'el-icon-s-grid',
        defaultStyle: {
          height: 150,
        },
      },
    ],
  },
];
