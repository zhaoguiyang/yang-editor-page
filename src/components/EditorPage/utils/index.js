import { cloneDeep, merge } from 'lodash';
import { createUUID, deepClone } from './hepler';

// 元素配置信息字段
const elementConfig = {
  name: '',
  scale: 1, // 页面的视图百分比，放大缩小
  // 页面初始样式
  commonStyle: {
    backgroundColor: '#f5f5f5',
    fontSize: 14,
    fontWeight: '',
  },
  author: '',
  width: 375,
  height: 644,
  elements: [], // 页面容器集合
};

// 获取组件并写入配置
const getElementConfig = function (element, extendStyle = {}) {
  const elementData = cloneDeep(element);
  const type = elementData.valueType || 'String'; // 默认string类型
  const dict = {
    Sting: '',
    Array: [],
    Object: {},
    Boolean: false,
    Number: 0,
    // 待扩展数据类型
  };
  const elementConfigData = cloneDeep(elementConfig);
  const config = {
    uuid: createUUID(),
    ...elementConfigData,
    elName: elementData.elName,
    elType: elementData.elType,
    propsValue: deepClone(elementData.needProps || {}),
  };
  // 样式
  config.commonStyle = merge(config.commonStyle, elementData.defaultStyle, element.commonStyle || {});
  config.commonStyle = merge(config.commonStyle, extendStyle);

  config.value = element.defaultValue || dict[type];
  config.valueType = type;
  config.isForm = !!element.isForm;
  return config;
};

/**
 * 获取元素样式并设置
 * @param styleObj
 * @param scalePoint 缩放比例
 */
const getCommonStyle = function (styleObj, scalingRatio = 1) {
  // 可以设置的元素属性
  const needUnitStr = ['width', 'height', 'top', 'left', 'paddingTop', 'paddingLeft',
    'paddingRight', 'paddingBottom', 'marginTop', 'marginLeft', 'marginRight', 'marginBottom', 'borderWidth',
    'fontSize', 'borderRadius', 'letterSpacing'];
  const style = {};

  for (const key in styleObj) {
    if (needUnitStr.includes(key)) {
      style[key] = `${styleObj[key] * scalingRatio}px`;
    } else {
      style[key] = styleObj[key];
    }
  }
  if (style.rotate) {
    style.transform = `rotate(${style.rotate}deg)`;
  }
  style.backgroundImage = style.backgroundImage ? `url(${style.backgroundImage})` : '';
  return style;
};
export {
  getElementConfig,
  getCommonStyle,
};
